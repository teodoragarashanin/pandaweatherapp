//
//  Entity+CoreDataProperties.swift
//  PandaWeatherApp
//
//  Created by Teodora on 8/28/16.
//  Copyright © 2016 Teodora. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Entity {
    @NSManaged var name: String?
}
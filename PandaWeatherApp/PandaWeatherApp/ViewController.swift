//
//  ViewController.swift
//  PandaWeatherApp
//
//  Created by Teodora on 8/28/16.
//  Copyright © 2016 Teodora. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON
import CoreLocation


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPageViewControllerDataSource, UIPageViewControllerDelegate, CLLocationManagerDelegate   {
    
    // MARK: Properties
    
    var listItems = [NSManagedObject]()
    var pageViewController: UIPageViewController?
    var refreshControl: UIRefreshControl!
    var locationManager = CLLocationManager()
    let geoCoder = CLGeocoder()
    
    // MARK: IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var refreshButton: UIButton!
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        saveAllItems()
        tableView.reloadData()
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(ViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(refreshControl)
         NSTimer.scheduledTimerWithTimeInterval(60*10, target: self, selector: #selector(ViewController.handleTimer(_:)), userInfo: nil, repeats: true)
        
}
    
    // MARK: Actions
    
  
    @IBAction func refreshButtonTapped(sender: UIButton) {
            self.tableView.reloadData()
    }
    
    // MARK: Location methods
    
    func handleTimer(timer: NSTimer) {
        if listItems.count >= 10 {
            locationManager.startUpdatingLocation()
            self.listItems.removeLast()
            saveCurrentLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        let location = CLLocation(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        geoCoder.reverseGeocodeLocation(location) {
            (placemarks, error) -> Void in
            
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            if let city = placeMark.addressDictionary!["City"] as? NSString {
                let currentCityName = city as String
                print(currentCityName)
                
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(currentCityName, forKey: "currentCityName")
                self.locationManager.stopUpdatingLocation()
        }
        }
    }
    
    func refresh(sender:AnyObject) {
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    // MARK: CoreData
    
    func saveItem (name:String) {
        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext: NSManagedObjectContext = appDelegate.managedObjectContext
        let item = NSEntityDescription.insertNewObjectForEntityForName("Entity", inManagedObjectContext: managedContext)
        item.setValue(name, forKey: "name")

        do {
            try managedContext.save()
            listItems.append(item)
        }
        catch {
            print("Error saving data")
        }
    }
    
    func saveCurrentLocation () {
        let defaults = NSUserDefaults.standardUserDefaults()
        if let name = defaults.stringForKey("currentCityName"){
            saveItem(name)
        }
    }
    
    func saveAllItems () {
            saveItem("Rome")
            saveItem("NewYork")
            saveItem("Moskow")
            saveItem("Paris")
            saveItem("Berlin")
            saveItem("Boston")
            saveItem("Nis")
            saveItem("Kragujevac")
            saveItem("Madrid")
            saveItem("Belgrade")
    }

    
    // MARK: UITableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")! as! TableViewCell
        let item = listItems[indexPath.row]
        cell.cityNameLabel?.text = item.valueForKey("name") as? String
        
        var urlString: String = "http://api.openweathermap.org/data/2.5/forecast/city?q="
        urlString = urlString.stringByAppendingString((item.valueForKey("name") as? String)!)
        urlString = urlString.stringByAppendingString("&APPID=5d2056eba9370b880b4e38d7be774bde")
        
        Alamofire.request(.GET, urlString) .responseJSON { response in // 1
            switch response.result {
            case .Success(let data):
                
                let weather = Weather(weatherJSON: JSON(data))
                cell.temperatureLabel?.text = weather.temperature + "F"
                
            case .Failure(let error):
                print("Error", error)
            }
        }
    
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        createPageViewController(indexPath.row)
        //setupPageControl()
        pageViewControllerAtIndex(indexPath.row)
        
    }
    
    // MARK: UIPageViewController
    
    func createPageViewController (index: Int) {
        let pageController: UIPageViewController = self.storyboard!.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
        pageController.dataSource = self
        pageController.delegate = self
        
        if listItems.count > 0 {
            
            let firstController = pageViewControllerAtIndex(index)!
            let startingViewControllers: [UIViewController] = [firstController]
            pageController.setViewControllers(startingViewControllers, direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        }
        
        pageViewController = pageController
        addChildViewController(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMoveToParentViewController(self)
    }
    
    func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.grayColor()
        appearance.currentPageIndicatorTintColor = UIColor.whiteColor()
        appearance.backgroundColor = UIColor.darkGrayColor()
    }
    
    func pageViewControllerAtIndex (pageIndex:Int) -> PageContentViewController? {
        
        if pageIndex < listItems.count {
            
            let pageContentViewController = self.storyboard!.instantiateViewControllerWithIdentifier("PageContentViewController") as! PageContentViewController
            
            pageContentViewController.pageIndex = pageIndex
            let item =  listItems[pageIndex].valueForKey("name") as! String
            pageContentViewController.cityName = item
            return pageContentViewController
            
        }
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let pageController = viewController as! PageContentViewController
        if pageController.pageIndex > 0 {
            return pageViewControllerAtIndex(pageController.pageIndex-1)
        }
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let pageController = viewController as! PageContentViewController
        if pageController.pageIndex+1 < listItems.count {
        return pageViewControllerAtIndex(pageController.pageIndex+1)
        }
        return nil
    }
    
    // MARK: - Page Indicator
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return listItems.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    
}


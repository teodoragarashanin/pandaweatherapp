//
//  Weather.swift
//  PandaWeatherApp
//
//  Created by Teodora on 8/28/16.
//  Copyright © 2016 Teodora. All rights reserved.
//

import Foundation
import SwiftyJSON

class Weather {
    
    // MARK: Properties
    
    var cityName = String ()
    var desc = String ()
    var temperature = String ()
    var firstTime = String ()
    var secondTime = String ()
    var pressure = String ()
    var rain = String ()
    var percentage = String ()
    var rainy = String ()
    var wind = String ()
    var country = String ()
    var mainImage = String ()
    
    // MARK: Designated Initializer
    
    init (weatherJSON: JSON) {
        
        self.cityName = weatherJSON["city"]["name"].stringValue
        self.desc = weatherJSON["list"][0]["weather"][0]["description"].stringValue
        self.temperature = weatherJSON["list"][0]["main"]["humidity"].stringValue
        self.firstTime = weatherJSON["list"][0]["weather"]["temp"].stringValue
        self.secondTime = weatherJSON["list"][0]["weather"]["temp"].stringValue
        self.pressure = weatherJSON["list"][0]["main"]["pressure"].stringValue
        self.wind = weatherJSON["list"][0]["wind"]["speed"].stringValue
        self.percentage = weatherJSON["list"][0]["main"]["humidity"].stringValue
        self.country = weatherJSON["city"]["country"].stringValue
        self.rainy = weatherJSON["cnt"].stringValue

    }
}

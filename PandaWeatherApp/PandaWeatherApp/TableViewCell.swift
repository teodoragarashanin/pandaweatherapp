//
//  TableViewCell.swift
//  PandaWeatherApp
//
//  Created by Teodora on 8/28/16.
//  Copyright © 2016 Teodora. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)    }

}

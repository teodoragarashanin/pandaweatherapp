//
//  PageContentViewController.swift
//  PandaWeatherApp
//
//  Created by Teodora on 8/28/16.
//  Copyright © 2016 Teodora. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PageContentViewController: UIViewController {

    // MARK: Properties
    
    var pageIndex: Int = 0
    var cityName: String = ""
    var refreshControl: UIRefreshControl!
    
    // MARK: IBOutlets
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var countyNameLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var firstTimeLabel: UILabel!
    @IBOutlet weak var secondTimeLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!

    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weather()
        
        // API Key: 5d2056eba9370b880b4e38d7be774bde
        // URL Beograd npr:: http://api.openweathermap.org/data/2.5/forecast/city?q=Belgrade&APPID=5d2056eba9370b880b4e38d7be774bde
    }

    //MARK: Actions
    
    @IBAction func exportButtonTapped(sender: UIButton) {
        
            let activityViewController = UIActivityViewController(
                activityItems: [self.cityName as NSString],
                applicationActivities: nil)
            
            presentViewController(activityViewController, animated: true, completion: nil)
        }
    
    //MARK: Parsing method
    
    var time: NSDate?
    
    func fetch(completion: () -> Void) {
        weather()
        completion()
    }
    
    func weather () {
        
        var urlString: String = "http://api.openweathermap.org/data/2.5/forecast/city?q="
        urlString = urlString.stringByAppendingString(cityName) 
        urlString = urlString.stringByAppendingString("&APPID=5d2056eba9370b880b4e38d7be774bde")
        
        
        Alamofire.request(.GET, urlString) .responseJSON { response in
            switch response.result {
            case .Success(let data):
                let weather = Weather(weatherJSON: JSON(data))
                self.cityNameLabel?.text = weather.cityName + " ,"
                self.pressureLabel?.text = weather.pressure
                let tempF = (weather.temperature as NSString).doubleValue
                let tempC = (tempF - 32.0)/1.8
                let tempCInteger = Int(tempC)
                let tempCString = String(tempCInteger)
                self.temperatureLabel?.text = tempCString + " ℃"
                self.windLabel?.text = weather.wind
                self.descriptionLabel?.text = weather.desc
                self.percentageLabel?.text = weather.percentage +  " %"
                self.countyNameLabel?.text = weather.country
                self.rainLabel?.text = weather.rainy

                let temp = weather.desc as NSString
                if  temp .isEqualToString("clear sky")   {
                    self.mainImageView.image = UIImage(named:"sun")
                    
                } else if temp .isEqualToString("broken clouds") || temp .isEqualToString("few clouds") {
                    self.mainImageView.image = UIImage(named:"m.png")

                } else if temp .isEqualToString("light rain") {
                    self.mainImageView.image = UIImage(named:"rain.png")
                    
                } else {
                    self.mainImageView.image = UIImage(named: "snow.png")
                }
            case .Failure(let error):
                print("Error", error)
            }
        }
    }
    
}



